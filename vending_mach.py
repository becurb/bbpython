#! /usr/local/bin/python

"""

You are in the kitchen with a product manager. He mentions his
cousin recently rebuilt a vending machine from the early Roman
Republic. Apparently minting coins was labor intensive, so the
designers tried to develop a change return mechanism that would
return the fewest number (of) coins necessary to meet the desired
change. They did this by:

always dispensing the largest denomination coin that was equal
or less than the remaining change amount first,

and then repeating the process on the remaining change amount.

While this worked for U.S. currency, it would sometimes fail for
certain amounts of Roman Currency (i.e. return a greater number of
coins than necessry), because the denominatiosn that the machine
would use as change were 1,4,5 and 9 cents.

Test the machine be requesting change in amounts of 0 to 1000 cents
(inclusively) - in how many cases would the change machine return
more coins than necessay?


Copyright 2015, Bruce Burden

"""

import re
import string
import fractions

################################################################################


failures = 0
bad_value = []

for i in range( 1001 ):
   value = i
   print "Change for: " + str( i )

   change = ""

   while( i != 0 ):
      if i in bad_value:
         failures += 1;
         bad_value.append( value )
         print "remaining change " + str( i ) \
         + " already shown to return more than the minimum number of coins"
         i = 0
         break

      if i >= 9:
         i = i - 9
         change += "9 "
         continue
      elif i >= 5:
         i = i - 5
         change += "5 "
         continue
      elif i >= 4:
         i = i - 4
         change += "4 "
         continue
      elif i >= 1:
         i = i - 1
         change += "1 "
         continue
      elif value == 0:
         change += "0"
         continue
      else:
         print "How did we get here?"

   if 3 < value:
      if fractions.gcd( 5, value ) == 5:
         if value / 5 < len( change ) / 2:
            print "5 is a GCD"
            bad_value.append( value )
            failures += 1

      if fractions.gcd( 4, value ) == 4:
         if value / 4 < len( change ) / 2:
            print "4 is a GCD"
            print "incorrect change returned for " + str( value )
            bad_value.append( value )
            failures += 1

   print change + "\n"

print "Number of times too many coins returned as change: " + str( failures )

